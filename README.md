# README

## Pré-requis

- Un compte GitLab
- Un compte sur Docker Hub
- Un token **Qovery** (`QOVERY_CLI_ACCESS_TOKEN`)

### Remarque: 

- `[💜 Qovery]`: vous êtes dans l'interface Qovery 
- `[🧡 GitLab]`: vous êtes dans l'interface GitLab

## [🧡 GitLab] Dockeriser l'application et builder & pousser l'image sur Docker Hub

- Forker le projet https://gitlab.com/devoxxfr-2023/env-tests/demo-web-app
- Supprimer la relation de fork
  - Aller dans **Settings/General/Advanced**
  - Puis **Remove fork relationship**

### "Ouvrir" le projet

> Vous pouvez utiliser 
> - Gitpod
> - Le WebIDE de GitLab
> - Votre IDE favoris (il faudra cloner le projet sur votre poste)

#### Settings

Dans les **Settings CI/CD** du projet, à la section **Variables**, ajouter les variables suivantes:

- `DOCKER_REGISTRY_PASSWORD` (cochez **mask variable**, décochez **protect variable**)
- `DOCKER_REGISTRY_USER` (décochez **protect variable**)
- `QOVERY_CLI_ACCESS_TOKEN` (cochez **mask variable**, décochez **protect variable**)

#### Pipeline pour construire et publier l'image

Ajouter un fichier `.gitlab-ci.yml` avec le contenu suivant:

```yaml
stages:
  - build-and-push

# --- Configuration ---

variables:
  DOCKER_REGISTRY: https://index.docker.io/v1/
  APPLICATION_NAME: demo-web-app
  DOCKER_REGISTRY_IMAGE: $DOCKER_REGISTRY_USER/${APPLICATION_NAME}

  # Kaniko
  JSON_CONFIG: |
    {
      "auths": {
        "$DOCKER_REGISTRY": {
          "username": "$DOCKER_REGISTRY_USER",
          "password": "$DOCKER_REGISTRY_PASSWORD"
        }
      }
    }

kaniko:build:push:
  stage: build-and-push
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  rules:
    # Production
    - if: $CI_COMMIT_BRANCH == "main"
      variables:
        DOCKER_IMAGE_TAG: ${CI_COMMIT_SHORT_SHA}
    # Development
    - if: $CI_MERGE_REQUEST_IID
      variables:
        DOCKER_IMAGE_TAG: ${CI_COMMIT_SHORT_SHA}
    # Release
    - if: $CI_COMMIT_TAG
      variables:
        DOCKER_IMAGE_TAG: ${CI_COMMIT_TAG}
  script: |
    echo "${JSON_CONFIG}" > /kaniko/.docker/config.json
    cat /kaniko/.docker/config.json
    /kaniko/executor \
      --context $CI_PROJECT_DIR \
      --dockerfile $CI_PROJECT_DIR/Dockerfile \
      --cache=true \
      --destination $DOCKER_REGISTRY_IMAGE:$DOCKER_IMAGE_TAG
```

Committez et pousser vos modifications:

```bash
git add .
git commit -m "🐳 first build"
git push
```

Cela va déclencher le pipeline, builder l'image et la publier sur le Docker Hub.

Dans les logs du runner, relevez le nom de l'image et de son tag: sous la forme de `k33g/demo-web-app:b7c09927` (*ceci est un exemple*)

## [💜 Qovery] Initialisation du projet & 1er déploiement

Dans votre **organisation** Qovery

### Créer une registry

- Allez dans les settings de votre organisation
- Cliquez sur **Container registries**
- Ajoutez une registry de type **DOCKER_HUB**
- Ajoutez vos credentials docker

### Créer le projet, l'environnement, et déployer l'application

- Créez un nouveau projet: `hello-devoxx`
- Créez un environnement pour ce projet: `env-webapp`
- Ajoutez (à l'environnement) un nouveau service: `demo-web-app`
  - Choisir **create an application**
  - name: `demo-web-app`
  - application source: choisir **Docker registry**:
    - choisir votre registry
    - Image name: `k33g/demo-web-app:b7c09927` (*ceci est un exemple*)
    - Image tag: `b7c09927` (*ceci est un exemple*)
    - cliquez sur **Continue**
    - laissez les paramètres de ressources en l'état, cliquez sur **Continue**
    - pour le port public exposé, ajoutez `8080`, cliquez sur **Continue**
    - et enfin, cliquez sur **Create and deploy**
- ⏳ Patientez, le déploiement est en cours
- Une fois l'application déployée, cliquez sur la ligne de l'application
  - Cliquez sur le bouton **Open links** pour obtenir l'URL de votre application
  - Vérifiez que l'application a bien été déployée


## [🧡 GitLab] 1er (re)déploiement de l'application avec GitLab CI et la CLI Qovery

Dans le fichier `.gitlab-ci.yml`:

### Stage

Ajoutez un stage `deploy`:

```yaml
stages:
  - build-and-push
  - deploy
```

### Configuration (variables)

Modifiez la section `variables` de la manière suivante:

```yaml
variables:
  DOCKER_REGISTRY: https://index.docker.io/v1/
  APPLICATION_NAME: demo-web-app
  DOCKER_REGISTRY_IMAGE: $DOCKER_REGISTRY_USER/${APPLICATION_NAME}
  # Qovery
  QOVERY_CLI_VERSION: 0.56.3
  QOVERY_ORGANIZATION: QoveryDevoxxDemo # 👋 bien penser à mettre le bon nom d'organisation
  QOVERY_PROJECT: hello-devoxx
  QOVERY_ENVIRONMENT: env-webapp
  # Kaniko
  JSON_CONFIG: |
    {
      "auths": {
        "$DOCKER_REGISTRY": {
          "username": "$DOCKER_REGISTRY_USER",
          "password": "$DOCKER_REGISTRY_PASSWORD"
        }
      }
    }

```

### Ajouter les "helpers" nécessaires

> à insérez avant la partie variables (après, cela doit fonctionner aussi)

#### CLI

Nous aurons besoin d'installer la **CLI Qovery**:

```yaml
.install-qovery-cli: &install-qovery-cli
  - echo "🤖 Installing wget... ⏳"
  - apt-get update
  - apt-get install wget -y
  - echo "🤖 Installing Qovery CLI... ⏳"
  - wget https://github.com/Qovery/qovery-cli/releases/download/v${QOVERY_CLI_VERSION}/qovery-cli_${QOVERY_CLI_VERSION}_linux_amd64.tar.gz
  - tar xvf qovery-cli_${QOVERY_CLI_VERSION}_linux_amd64.tar.gz
  - echo "🤖 Qovery CLI installed 🎉"
```

#### Déployer un container

Nous aurons besoin de déployer le container sur **Qovery** (avec la CLI):

```yaml
.deploy-container: &deploy-container |
  echo "🚀 deploying application..." 
  ./qovery container deploy \
    --organization ${QOVERY_ORGANIZATION} \
    --project ${QOVERY_PROJECT} \
    --environment ${CURRENT_QOVERY_ENVIRONMENT} \
    --container ${APPLICATION_NAME} \
    --tag ${DOCKER_IMAGE_TAG} \
    --watch
```

#### Obtenir l'URL de l'application une fois déployée

Et voici un **"petit hack"** pour obtenir l'url de l'application déployé
> si vous utilisez vos propres noms de domaine, c'est plus simple car vous saurez comment construire vos URLs à l'avance

```yaml
.update-environment-url: &update-environment-url |
  echo "🌍 building url..." 
  LINE=$(./qovery container domain list -n ${APPLICATION_NAME} \
    --organization ${QOVERY_ORGANIZATION} \
    --project ${QOVERY_PROJECT} \
    --environment ${CURRENT_QOVERY_ENVIRONMENT} | tail -n +2)
  LINE=$(echo "${LINE}" | grep -o -P '(?<=\|).*(?=\|)')
  
  var1=${LINE#?????????}
  var2=${var1%???????????}

  DYNAMIC_ENVIRONMENT_URL="https://${var2}"
  echo "DYNAMIC_ENVIRONMENT_URL=${DYNAMIC_ENVIRONMENT_URL}" >> deploy.env
```

#### Clôner un environnement

Nous devons pouvoir clôner l'environnement de départ:
> Notamment lorsque l'on fait une merge request

```yaml
.clone-environment-if-not-exists-and-mr: &clone-environment-if-not-exists-and-mr |
  if [ -n "$CI_MERGE_REQUEST_IID" ]
  then
    echo "🤖 cloning environment..."    
    ./qovery environment clone \
      --organization ${QOVERY_ORGANIZATION} \
      --project ${QOVERY_PROJECT} \
      --environment ${QOVERY_ENVIRONMENT} \
      --new-environment-name ${CURRENT_QOVERY_ENVIRONMENT} || true
  fi
```

#### Supprimer un environnement

Nous devons pouvoir supprimer un environnement:

```yaml
.remove-environment: &remove-environment |
  echo "🤚 deleting environment..."
  ./qovery environment delete \
    --organization ${QOVERY_ORGANIZATION} \
    --project ${QOVERY_PROJECT} \
    --environment ${QOVERY_ENVIRONMENT}-${CI_COMMIT_REF_NAME} \
    --watch
```

### Ajoutez le job de déploiement

À la suite du job de build, ajoutez le job de déploiement sur Qovery:

```yaml
qovery:deploy:
  stage: deploy
  image: debian:buster-slim
  rules:
    # Production
    - if: $CI_COMMIT_BRANCH == "main"
      variables:
        DOCKER_IMAGE_TAG: ${CI_COMMIT_SHORT_SHA}
        CURRENT_QOVERY_ENVIRONMENT: ${QOVERY_ENVIRONMENT}
    # Development
    - if: $CI_MERGE_REQUEST_IID
      variables:
        DOCKER_IMAGE_TAG: ${CI_COMMIT_SHORT_SHA}
        CURRENT_QOVERY_ENVIRONMENT: ${QOVERY_ENVIRONMENT}-${CI_COMMIT_REF_NAME}
    # Release
    - if: $CI_COMMIT_TAG
      variables:
        DOCKER_IMAGE_TAG: ${CI_COMMIT_TAG}
        CURRENT_QOVERY_ENVIRONMENT: ${QOVERY_ENVIRONMENT}
  artifacts:
    reports:
      dotenv: deploy.env
  environment:
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}
    url: $DYNAMIC_ENVIRONMENT_URL
    on_stop: qovery:remove:environment
  before_script:
    - *install-qovery-cli
  script:
    - *clone-environment-if-not-exists-and-mr
    - *deploy-container
    - *update-environment-url
```

### Ajouter le job de décommissionnement de l'environnement clôné

Et enfin ajoutez le job de suppression d'environnement
> qui se déclenchera au moment du merge de la merge request pour redéployer les changement en "production"

```yaml
qovery:remove:environment:
  stage: deploy
  image: debian:buster-slim
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual
    - if: $CI_COMMIT_BRANCH == "main"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
  environment:
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}
    action: stop
  allow_failure: true
  # Install the Qovery CLI
  before_script:
    - *install-qovery-cli
  script:
    - *remove-environment
```

**👋 NE COMMITTEZ PAS TOUT DE SUITE!**

### Modifier la page `index.html`

- Allez modifier le code de `public/index.html` pour afficher un nouveau message
- Committez et poussez vos changements
- ⏳ allez observer le pipline et les logs des runners
- Au bout de quelques instants, votre nouvelle application est déployée 🎉

> Si vous allez dans la rubrique **Environnement de GitLab**, vous pouvez voir que votre application a été publiée, et vous avez le lien (URL) pour y accéder

## [🧡 GitLab] Review(Preview) application (et clône d'environnement)

- Créez une nouvelle branche du projet (à partir de GitLab)
- Créez une merge request (à partir de GitLab et de cette branche)
- Apportez des modifications, sur cette branche, (dans `public/index.html`)
- Committez puis poussez vos modifications
- ⏳ allez observer la merge request, le pipline et les logs des runners
- Au bout de quelques instants, votre nouvelle **Review** application est déployée 🎉

> - À partir de la page de la Merge Request, vous avez le lien vers la **Review** application
> - Vous trouverez le même lien dans la liste des environnements

### Une fois la review application déployée

Si vous êtes satisfaits, vous pouvez enlever le mode **Draft** de la MR, et merger. Cela va déclencher:

- la suppression de l'environnement de review
- le redéploiement de l'application (avec les modifs) en "production"

